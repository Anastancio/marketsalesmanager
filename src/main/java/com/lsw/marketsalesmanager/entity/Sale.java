package com.lsw.marketsalesmanager.entity;

import com.lsw.marketsalesmanager.enums.Category;
import com.lsw.marketsalesmanager.enums.DealMethod;
import com.lsw.marketsalesmanager.enums.DealType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate date;

    @Column(nullable = false)
    private LocalTime time;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private DealType dealType;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private DealMethod dealMethod;

    @Column(nullable = false,length = 10)
    private String code;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Category category;

    @Column(nullable = false,length = 20)
    private String name;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Integer unitPrice;

    private String ect;



}
