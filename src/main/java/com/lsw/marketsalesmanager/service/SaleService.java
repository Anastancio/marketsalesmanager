package com.lsw.marketsalesmanager.service;

import com.lsw.marketsalesmanager.entity.Sale;
import com.lsw.marketsalesmanager.enums.DealType;
import com.lsw.marketsalesmanager.model.SaleItem;
import com.lsw.marketsalesmanager.model.SaleRequest;
import com.lsw.marketsalesmanager.repository.SaleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SaleService {

    private final SaleRepository saleRepository;

    public int setSale(SaleRequest request){
        Sale addData = new Sale();
        addData.setDealType(request.getDealType());
        addData.setDealMethod(request.getDealMethod());
        addData.setCode(request.getCode());
        addData.setCategory(request.getCategory());
        addData.setName(request.getName());
        addData.setQuantity(request.getQuantity());
        addData.setUnitPrice(request.getUnitPrice());
        addData.setEct(request.getEct());

        saleRepository.save(addData);

        return request.getQuantity() * request.getUnitPrice();

    }

    public List<SaleItem> getSales() {
        List<Sale> originList = saleRepository.findAll();

        List<SaleItem> result = new LinkedList<>();



    }
}
