package com.lsw.marketsalesmanager.model;

import com.lsw.marketsalesmanager.enums.Category;
import com.lsw.marketsalesmanager.enums.DealMethod;
import com.lsw.marketsalesmanager.enums.DealType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SaleRequest {

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DealType dealType;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DealMethod dealMethod;

    @NotNull
    @Length(min = 10,max = 10)
    private String code;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Category category;

    @NotNull
    @Length(min = 1, max = 20)
    private String name;

    @NotNull
    private Integer quantity;

    @NotNull
    private Integer unitPrice;

    private String ect;



}
