package com.lsw.marketsalesmanager.model;

import com.lsw.marketsalesmanager.enums.Category;
import com.lsw.marketsalesmanager.enums.DealMethod;
import com.lsw.marketsalesmanager.enums.DealType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class SaleItem {
    private Long id;

    private LocalDate date;

    private LocalTime time;

    private DealType dealType;

    private DealMethod dealMethod;

    private String code;

    private Category category;

    private String name;

    private Integer quantity;

    private Integer unitPrice;

    private String ect;

    private Integer dealPrice; //실거래금액

    private Double vat; //부가세

    private Double vos; //공급가액


}
