package com.lsw.marketsalesmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {

    FOOD("식품"),
    DRINK("음료"),
    WARM_THING("방한용품");
    private final String categoryName;

}
