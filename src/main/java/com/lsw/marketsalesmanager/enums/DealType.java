package com.lsw.marketsalesmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DealType {

    SELL("판매"),

    CANCEL("취소");

    private final String dealTypeName;
}
