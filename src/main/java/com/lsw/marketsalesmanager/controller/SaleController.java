package com.lsw.marketsalesmanager.controller;

import com.lsw.marketsalesmanager.model.SaleRequest;
import com.lsw.marketsalesmanager.service.SaleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sale")
public class SaleController {

    private final SaleService saleService;

    @PostMapping("/data")
    public int setSale(@RequestBody @Valid SaleRequest request) {
        int result = saleService.setSale(request);

        return result;
    }


}
